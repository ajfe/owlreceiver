#!/bin/env python

import pandas as pd
from numpy import mean

WEEKDAYS=['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday']
MONTHS=['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']

def month_to_name(n):
    return MONTHS[n-1]

def weekday_to_name(n):
    return WEEKDAYS[n]

def main():
    
    # df = pd.DataFrame(data = [('Bob', 968), ('Jessica', 155), ('Mary', 77), ('John', 578), ('Mel', 973)], columns=['Names', 'Births'])
    pwrdata = pd.read_csv('all.csv', sep=';', parse_dates=['date'], index_col='date')
    
    pwrdata['hour'] = pwrdata.index.hour
    pwrdata['weekday'] = pwrdata.index.weekday
    pwrdata['monthday'] = pwrdata.index.day
    pwrdata['monthname'] = [ MONTHS[i-1] for i in pwrdata.index.month ] # names
    pwrdata['month'] = pwrdata.index.month # numbers
    #pwrdata['month-weekday'] = [ "{0}-{1}".format(i.month, i.weekday) for i in pwrdata.index ]
    
    agg_by_month_weekday = pwrdata.groupby(['month', 'weekday']).aggregate([sum,mean])
    agg_by_month_weekday.index.set_levels([[ month_to_name(i) for i in agg_by_month_weekday.index.levels[0] ],
                                          [ weekday_to_name(i) for i in agg_by_month_weekday.index.levels[1]]],
                                          inplace=True)
    #print agg_by_month_weekday['hour_pwr_use']
    
    for idx, row in agg_by_month_weekday.iterrows():
        print idx
        print row['hour_pwr_use']['sum']
    
#     agg_by_month = pwrdata.groupby('month').aggregate(mean)
#     agg_by_month.index = [ MONTHS[i-1] for i in agg_by_month.index ] #number-to-name
#     print agg_by_month['hour_pwr_use']
#         
#     agg_by_hour = pwrdata.groupby('hour').aggregate(mean)
#     print agg_by_hour['hour_pwr_use']
#     
#     agg_by_weekday = pwrdata.groupby('weekday').aggregate(mean)
#     agg_by_weekday.index = WEEKDAYS
#     print agg_by_weekday['hour_pwr_use']
#     
#     agg_by_monthday = pwrdata.groupby('monthday').aggregate(mean)
#     print agg_by_monthday['hour_pwr_use']
    

if __name__ == '__main__':
    main()