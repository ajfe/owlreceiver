#!/bin/bash

. /home/ajfe/virtualenvs/owl/bin/activate

nohup ./owlreceiver.py -U > owlreceiver.out 2>&1 &

OWLPID=$!

echo "OWL receiver started with PID $OWLPID"
ps -ef | grep owlreceiver | grep -v grep
