#!/usr/bin/env python
from EmonCMS import EmonCMS
from datetime import datetime

def main():
    input_file = "/home/ajfe/hostshare/443719101897_export.csv"
        
    emon = EmonCMS("http://emoncms.petisco", "c8dc9eb90d1596e75c4b902faf97c7ae", 5)
    node = "mimi2"
    data_template = '''{{ "instant_pwr" : {0}, "cumulative_pwr" : {1} }}'''
    
    with open(input_file) as f:
        lines = f.readlines()
    
    for line in lines:
        parts = line.split(",")
        dt_formatted = parts[0]
        instant_pwr = parts[1]
        cumulative_pwr = parts[3]
        
        data = data_template.format(instant_pwr, cumulative_pwr)
        
        dt = datetime.strptime(dt_formatted, '%Y-%m-%d %H:%M:%S')
        data_time = int((dt - datetime(1970, 1, 1)).total_seconds())
        
        j = emon.post(node, data, data_time)
        print j
    

if __name__ == '__main__':
    main()