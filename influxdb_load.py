#!/usr/bin/env python
import sqlite3
import datetime
import pytz
import requests


def str_to_time(time_str):
    time_str_notz = time_str.split("+")[0]
    utc = datetime.datetime.strptime(time_str_notz,"%Y-%m-%d %H:%M:%S")
    utc = utc.replace(tzinfo=pytz.utc)
    return utc

def dt_to_nanos(dt):
    epoch = datetime.datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)

    return int((dt - epoch).total_seconds() * 1000000000)

def chunks(lst, n):
    """Yield successive n-sized chunks from lst."""
    for i in xrange(0, len(lst), n):
        yield lst[i:i + n]


class SQLiteDB:
    def __init__(self,dbfile):
        self.conn = sqlite3.connect(dbfile) #,detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row
        
    def close(self):
        self.conn.close()

    def readElectricityDay(self):
        
        cur = self.conn.cursor()
        cur.execute("""select time, curr_power_usage, curr_power_cost_h, day_cum_power_usage, day_cum_power_cost 
                        from owlelectricity;""")
        
        el_data = []
        
        rows = cur.fetchall()
        for row in rows:
            (time_str, curr_pwr, curr_cost, day_pwr, day_cost) = (row)
            time = str_to_time(time_str)
            el = Elec(time,curr_pwr,curr_cost,day_pwr,day_cost)
            el_data.append(el)
            
        return el_data

class InfluxRecord:
    def __init__(self, measurement, tags_dict, fields_dict, time):
        self.measurement = measurement
        self.tags_dict = tags_dict
        self.fields_dict = fields_dict
        self.time = time

    # cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000
    def __str__(self):
        tags_str = ",".join(["{0}={1}".format(tag_k, tag_v) for (tag_k, tag_v) in self.tags_dict.iteritems() ])
        fields_str = ",".join(["{0}={1}".format(field_k, field_v) for (field_k, field_v) in self.fields_dict.iteritems() ])

        time_ns = dt_to_nanos(self.time)

        return "{0},{1} {2} {3}".format(self.measurement, tags_str, fields_str, time_ns)

class InfluxAPI:
    def __init__(self, url, database):
        self.url = url
        self.database = database

    def write(self, data):

        write_api_url = "{0}/write".format(self.url)
        db_query_args = { "db" : self.database }

        post_data = "\n".join([ str(d) for d in data ])

        requests.post(write_api_url, params=db_query_args, data=post_data)

class Elec:
    def __init__(self,time,power_usage,power_cost,day_power_usage,day_power_cost):
        self.time = time
        self.power_usage = float(power_usage)
        self.power_cost = float(power_cost)
        self.day_power_usage = float(day_power_usage)
        self.day_power_cost = float(day_power_cost)
        
    def __str__(self):
        return "time={0}, pwr_use={1}, day_pwr_use={2}".format(self.time,self.power_usage,self.day_power_usage)

if __name__ == '__main__':
    db = SQLiteDB("owlreceiver.db")
    influx = InfluxAPI("http://localhost:8086", "telegraf")
    
    data = db.readElectricityDay()
    
    # sort data by d.time asc
    data.sort(key=lambda x: x.time)
    
    influxdata = []
    for d in data:
        tags = { "host" : "owl" }
        fields = { "current_power_usage" : d.power_usage,
                   "cumulative_energy_day" : d.day_power_usage }
        influxdata.append(InfluxRecord("owl_power_usage", tags, fields, d.time))

    influxdata_chunks = chunks(influxdata, 3000)

    for ch in influxdata_chunks:
        loaded = len(ch)
        influx.write(ch)
        print("{0} records loaded".format(loaded))
