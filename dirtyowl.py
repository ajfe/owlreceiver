#!/usr/bin/env python
import sqlite3
import datetime
import pytz


def str_to_time(time_str):
    time_str_notz = time_str.split("+")[0]
    utc = datetime.datetime.strptime(time_str_notz,"%Y-%m-%d %H:%M:%S")
    utc = utc.replace(tzinfo=pytz.utc)
    return utc_to_local(utc)

def utc_to_local(utc_time):
    local = pytz.timezone ("Europe/Lisbon")
    local_dt = utc_time.astimezone(local)
    return local_dt

def utc_to_local_str(utc_time_str, time_fmt="%Y-%m-%d %H:%M:%S"):
    utc = datetime.datetime.strptime (utc_time_str, time_fmt)
    local_str = local_to_utc(utc).strftime(time_fmt)
    return local_str
    

def local_to_utc(local_time):
    local = pytz.timezone ("Europe/Lisbon")
    local_dt = local.localize(local_time, is_dst=None)
    utc_dt = local_dt.astimezone (pytz.utc)
    return utc_dt

def local_to_utc_str(local_time_str, time_fmt="%Y-%m-%d %H:%M:%S"):
    local = datetime.datetime.strptime (local_time_str, time_fmt)
    utc_str = local_to_utc(local).strftime(time_fmt)
    return utc_str

class SQLiteDB:
    def __init__(self,dbfile):
        self.conn = sqlite3.connect(dbfile) #,detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row
        
    def close(self):
        self.conn.close()

    def readElectricityDay(self):
        
        start_str = local_to_utc_str("2016-07-01 00:00:00")
        end_str = local_to_utc_str("2016-07-02 00:00:00")
        
#         start_str = "2016-07-06 23:00:00"
#         end_str = "2016-07-07 23:00:00"
        cur = self.conn.cursor()
        cur.execute("""select time, curr_power_usage, curr_power_cost_h, day_cum_power_usage, day_cum_power_cost 
                        from owlelectricity 
                        where datetime(time) >= ? and datetime(time) < ?""",(start_str,end_str)) ## UTC
        
#         where time >= ? and time < ?""",(1467849600,1467936000))
        
        el_data = []
        
        el_data.append(Elec(str_to_time(start_str),0.0,0.0,0.0,0.0))
        
        rows = cur.fetchall()
        for row in rows:
            (time_str, curr_pwr, curr_cost, day_pwr, day_cost) = (row)
            time = str_to_time(time_str)
            el = Elec(time,curr_pwr,curr_cost,day_pwr,day_cost)
            el_data.append(el)
            
        return el_data

class Elec:
    def __init__(self,time,power_usage,power_cost,day_power_usage,day_power_cost):
        self.time = time
        self.power_usage = float(power_usage)
        self.power_cost = float(power_cost)
        self.day_power_usage = float(day_power_usage)
        self.day_power_cost = float(day_power_cost)
        
    def __str__(self):
        return "time={0}, pwr_use={1}, day_pwr_use={2}".format(self.time,self.power_usage,self.day_power_usage)

if __name__ == '__main__':
    db = SQLiteDB("owldb.db")
    
    data = db.readElectricityDay()
    
    # sort data by d.time asc
    data.sort(key=lambda x: x.time)
    
    prev = data[0]
    last = data[-1]
    cumulative_wattseconds = 0
    
    for d in data:
        print d
        dt = (d.time - prev.time).total_seconds() #seconds
        wattseconds = d.power_usage * dt * 1.0
        cumulative_wattseconds += wattseconds
        
        prev = d
        
    cumulative_wh = (cumulative_wattseconds / 3600.0)
    
    print "{0:0.2f} vs. {1}".format(cumulative_wh,last.day_power_usage)
