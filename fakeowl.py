#!/usr/bin/env python
import socket
import sys
from docopt import docopt
import time
import random

OWL_MCAST_GRP="224.192.32.19"
OWL_MCAST_PORT="22600"

docopt_str = '''fakeowl
sends fake <electricity> owl information to the multicast group

Usage:
    fakeowl [-g <multicast_group>] [-p <port>]
    fakeowl (-h | --help)
    fakeowl (-v | --version)
   
Options:
    -g --group=<multicast_group>    Multicast group to send to [default: {0}]
    -p --port=<port>                UDP port to listen on. [default: {1}]
    -h --help                       Show this screen.
    -v --version                    Show version.
    
'''.format(OWL_MCAST_GRP,OWL_MCAST_PORT)

class MulticastSender:
    def __init__(self,mcast_group,mcast_port):
        self.mcast_group = mcast_group
        self.mcast_port = int(mcast_port)
        self.open()
        
    def open(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.IPPROTO_IP, socket.IP_MULTICAST_TTL, 2)
        
        self.the_socket = sock
        
    def send(self,payload):
        self.the_socket.sendto(payload, (OWL_MCAST_GRP, int(OWL_MCAST_PORT)))
    
    def close(self):
        self.the_socket.close()
    
    def reopen(self):
        self.close()
        self.open()
        
class FakeOwl:
    xml_template = """<electricity id="112233445566" ver="2.0"><timestamp>{0}</timestamp><signal rssi="-67" lqi="28"/><battery level="100%"/><channels><chan id="0"><curr units="w">{1}</curr><day units="wh">{2}</day></chan><chan id="1"><curr units="w">0.00</curr><day units="wh">0.00</day></chan><chan id="2"><curr units="w">0.00</curr><day units="wh">0.00</day></chan><chan id="3"><curr units="w">0.00</curr><day units="wh">0.00</day></chan><chan id="4"><curr units="w">0.00</curr><day units="wh">0.00</day></chan><chan id="5"><curr units="w">0.00</curr><day units="wh">0.00</day></chan></channels><property><current><watts>{1}</watts><cost>5.95</cost></current><day><wh>{2}</wh><cost>146.96</cost></day><tariff><curr_price>0.19</curr_price><block_limit>4294967295</block_limit><block_usage>5321</block_usage></tariff></property></electricity>"""
#     xml_template = """<electricity id="112233445566" ver="2.0">
#                           <timestamp>{0}</timestamp>
#                           <signal rssi="-67" lqi="28"/>
#                           <battery level="100%"/>
#                           <channels>
#                             <chan id="0">
#                               <curr units="w">{1}</curr>
#                               <day units="wh">{2}</day>
#                             </chan>
#                             <chan id="1">
#                               <curr units="w">0.00</curr>
#                               <day units="wh">0.00</day>
#                             </chan>
#                             <chan id="2">
#                               <curr units="w">0.00</curr>
#                               <day units="wh">0.00</day>
#                             </chan>
#                             <chan id="3">
#                               <curr units="w">0.00</curr>
#                               <day units="wh">0.00</day>
#                             </chan>
#                             <chan id="4">
#                               <curr units="w">0.00</curr>
#                               <day units="wh">0.00</day>
#                             </chan>
#                             <chan id="5">
#                               <curr units="w">0.00</curr>
#                               <day units="wh">0.00</day>
#                             </chan>
#                           </channels>
#                           <property>
#                             <current>
#                               <watts>{1}</watts>
#                               <cost>5.95</cost>
#                             </current>
#                             <day>
#                               <wh>{2}</wh>
#                               <cost>146.96</cost>
#                             </day>
#                             <tariff>
#                               <curr_price>0.19</curr_price>
#                               <block_limit>4294967295</block_limit>
#                               <block_usage>5321</block_usage>
#                             </tariff>
#                           </property>
#                         </electricity>"""
    
    
    def __init__(self,sender):
        self.sender = sender
        self.day_usage = 0.0
        
    def getRandomPwr(self):
        rand_pwr = random.randrange(10000, 100000, 1)
        return rand_pwr / 100.0
    
    def twoDecimals(self,n):
        return "%.2f" % round(n,2)
    
    def genNextPacket(self):
        timestamp = int(time.time())
        curr_power = self.getRandomPwr()
        day_usage = self.day_usage + (curr_power / 60.0) #watt-hour
        self.day_usage = day_usage
        return FakeOwl.xml_template.format(str(timestamp),self.twoDecimals(curr_power),self.twoDecimals(day_usage))
    
    def send(self):
        nextpacket = self.genNextPacket()
        self.sender.send(nextpacket)
    
#   <electricity id="443719101897" ver="2.0">
#     <timestamp>1465940288</timestamp>
#     <signal rssi="-67" lqi="28"/>
#     <battery level="100%"/>
#     <channels>
#       <chan id="0">
#         <curr units="w">305.00</curr>
#         <day units="wh">6258.98</day>
#       </chan>
#       <chan id="1">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="2">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="3">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="4">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="5">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#     </channels>
#     <property>
#       <current>
#         <watts>305.00</watts>
#         <cost>5.95</cost>
#       </current>
#       <day>
#         <wh>6258.98</wh>
#         <cost>146.96</cost>
#       </day>
#       <tariff>
#         <curr_price>0.19</curr_price>
#         <block_limit>4294967295</block_limit>
#         <block_usage>5321</block_usage>
#       </tariff>
#     </property>
#   </electricity>


        

def main(args):
    s = None

    try:
        
        mcast_grp = args["--group"]
        mcast_port = int(args["--port"]) 
        
        s = MulticastSender(mcast_grp,mcast_port)
        fakeowl = FakeOwl(s)
    
        while True:
            fakeowl.send()
            sys.stdout.write('.')    
            sys.stdout.flush()
            time.sleep(60)

    except (KeyboardInterrupt):
        if s is not None:
            s.close()
            
    except ValueError as e:
        print e
        print "BAD port: {0}".format(args["--port"])
        sys.exit(2)
    finally:
        if s is not None:
            s.close()

if __name__ == '__main__':
    arguments = docopt(docopt_str, version='Owl Receiver 0.1')
    main(arguments)