#!/usr/bin/env python
from __future__ import print_function
from docopt import docopt
import requests
import json
import datetime
import pytz

docopt_str = '''emoncms
Simple HTTP client for emonCMS

Usage:
    emoncms -U <url> [-t <timeout>] -K <apikey> -N <node> -D <data> [-T <time>]
    emoncms (-h | --help)
    emoncms (-v | --version)
   
Options:
    -U --url=<url>              EmonCMS URL (example: http://emoncms.local)
    -t --timeout=<timeout>      HTTP timeout  [default: 5]
    -K --apikey=<apikey>        EmonCMS APIKEY
    -N --node=<node>            EmonCMS node name
    -D --data=<data>            Data to post (JSON)
    -T --time=<time>            Data timestamp (seconds since epoch).
    -h --help                   Show this screen.
    -v --version                Show version.
'''

THE_VERSION="EmonCMS 1.0"

class EmonCMS(object):

    def __init__(self, url, apikey, timeout):
        self.url = "{0}/emoncms/input/post".format(url)
        self.apikey = apikey
        self.timeout = timeout
        
    def post(self, node, data, time=None):
        postdata = { "node": node,
                    "fulljson": data,
                    "apikey": self.apikey
                    }
        if time is not None:
            if isinstance(time, datetime.datetime):
                timestamp = int((time - datetime.datetime(1970,1,1, tzinfo=pytz.utc)).total_seconds()) # convert to seconds since epoch
                postdata["time"] = timestamp

            else: # int or str
                postdata["time"] = time
            
        r = requests.post(self.url, data=postdata)
        
        return r.text
        
def print_indented_json(j):
    print(json.dumps(j,indent=4, sort_keys=True))
    
def main(args):
    emon_url = args["--url"]
    http_timeout = args["--timeout"]
    emon_node = args["--node"]
    emon_apikey = args["--apikey"]
    data = args["--data"]
    data_time = args["--time"]
    
    emon = EmonCMS(emon_url, emon_apikey, http_timeout)
    
    j = emon.post(emon_node, data, data_time)
    print(j)

if __name__ == '__main__':
    arguments = docopt(docopt_str, version=THE_VERSION)
    main(arguments)