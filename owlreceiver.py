#!/usr/bin/env python
import socket
import struct
import sys
from docopt import docopt
from Queue import Queue
from threading import Thread, Event
import xml.etree.ElementTree as ET
import datetime
import time
import os
import sqlite3
import pytz
import logging
#from EmonCMS import EmonCMS
import json
from requests.exceptions import RequestException
import requests

#EMON_URL="http://emoncms.petisco"
#EMON_APIKEY="c8dc9eb90d1596e75c4b902faf97c7ae"

## TODO Stuff
## - proper logging (arg: loglevel + logfile)
## - match owl's CSV fields? 
##   timestamp,curr_property,curr_property_cost,day_property,day_property_cost,curr_solar_generating,curr_solar_export,day_solar_generated,day_solar_export,curr_chan1,curr_chan2,curr_chan3,day_chan1,day_chan2,day_chan3
## - proper ORM :P
## - proper daemonizing
## - other owl object types (pv, weather, etc.)
## add emoncms command-line args
## config file??

THE_VERSION="Owl Receiver 0.5"

basedir = os.path.abspath(os.path.dirname(sys.argv[0]))
dbfile = basedir+'/owlreceiver.db'


OWL_MCAST_GRP="224.192.32.19"
OWL_MCAST_PORT="22600"

docopt_str = '''owlreceiver
Receiver for multicast data pushed by Owl Intuition network devices.

Usage:
    owlreceiver [-U] [-p <port>]
    owlreceiver [-M <multicast_group>] [-p <port>]
    owlreceiver (-h | --help)
    owlreceiver (-v | --version)
   
Options:
    -U --udp                                  UDP mode
    -M --multicast-group=<multicast_group>    Multicast group to join [default: {0}]
    -p --port=<port>                          UDP port to listen on. [default: {1}]
    -h --help                                 Show this screen.
    -v --version                              Show version.
    
'''.format(OWL_MCAST_GRP,OWL_MCAST_PORT)

class NetworkReader:
    def open(self):
        pass
    
    def read(self,recv_bytes=2048):
        return self.the_socket.recv(recv_bytes)
    
    def close(self):
        self.the_socket.close()
    
    def reopen(self):
        self.close()
        self.open()

class MulticastReader(NetworkReader):
    def __init__(self,mcast_group,mcast_port):
        self.mcast_group = mcast_group
        self.mcast_port = int(mcast_port)
        self.open()
        
    def open(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.mcast_group, self.mcast_port))  # use MCAST_GRP instead of '' to listen only
                                    # to MCAST_GRP, not all groups on MCAST_PORT
        mreq = struct.pack("4sl", socket.inet_aton(self.mcast_group), socket.INADDR_ANY)

        sock.setsockopt(socket.IPPROTO_IP, socket.IP_ADD_MEMBERSHIP, mreq)
        
        self.the_socket = sock


class UDPReader(NetworkReader):
    def __init__(self,udp_port):
        self.udp_addr = "" # all interfaces
        self.udp_port = int(udp_port)
        self.open()
        
    def open(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind((self.udp_addr, self.udp_port))
        
        self.the_socket = sock




        
class StoppableThread(Thread):

    def __init__(self):
        super(StoppableThread, self).__init__()
        self._stop = Event()

    def stop(self):
        self._stop.set()

    def stopped(self):
        return self._stop.isSet()
        
class ReaderThread(StoppableThread):
    def __init__(self,queue,reader):
        super(ReaderThread, self).__init__()
        self.queue = queue
        self.reader = reader
        self.name = "ReaderThread"
        self.daemon = True
        
    def run(self):
        while not self.stopped():
            item = self.reader.read()
            self.queue.put(item)

class ProcessorThread(StoppableThread):
    def __init__(self, in_queue, db, out_queue):
        super(ProcessorThread, self).__init__()
        self.queue = in_queue
        self.out_queue = out_queue
        self.dbfile = db
        self.name = "ProcessorThread"
        self.daemon = True        
    
    def run(self):
        db = SQLiteDB(self.dbfile)

        while not self.stopped():
            item_xml = self.queue.get()
            self.queue.task_done()
            owl_obj = OwlFactory.makeOwlObject(item_xml)
            
            if owl_obj is not None:
                if isinstance(owl_obj, OwlElectricity): ## make smarter, no ifs
                    db.storeElectricity(owl_obj)
                    logging.getLogger('default').debug("Wrote owlelectricity object: {0}".format(owl_obj.curr_power_usage))

                    self.out_queue.put(owl_obj)
                    logging.getLogger('default').debug("Re-Enqueued owlelectricity object: {0}".format(owl_obj.curr_power_usage))
                    
                elif isinstance(owl_obj, OwlWeather):
                    logging.getLogger('default').debug("Discarded owlweather object")
                else:
                    logging.getLogger('default').error("Unknown owlobject: {0}".format(owl_obj.__class__.__name__))
            else:
                logging.getLogger('default').error("No owlobject: None")    



## peewee DB sample code
# def connectToDb():
#     db = SqliteDatabase(dbfile)
#     db.connect()
#     #db.create_table(Article, safe=True)
#     #database_proxy.initialize(db)
#     return db
# 
# # shit is awful yo
# database_proxy = connectToDb()
# 
# 
# 
# # link, title, image URL, publish date
# class Article(Model):
#     url = CharField(primary_key=True)
#     title = CharField()
#     imageUrl = CharField()
#     publishDate = DateTimeField(default=datetime.datetime.now())
#     type = CharField(default="regular")
#     
# (...)
# 
#     class Meta:
#         database = database_proxy
##

class SQLiteDB:
    def __init__(self,dbfile):
        self.conn = sqlite3.connect(dbfile,detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row
        self.createTables()
        
    def close(self):
        self.conn.close()

    def storeElectricity(self,owl_electricity):
        try:

            cur = self.conn.cursor()
            cur.execute("""INSERT INTO owlelectricity ( device_id, 
                                                        time, 
                                                        curr_power_usage, 
                                                        curr_power_cost_h, 
                                                        day_cum_power_usage, 
                                                        day_cum_power_cost,
                                                        chan_curr_power_usage_0,
                                                        chan_cum_power_cost_0,
                                                        chan_curr_power_usage_1,
                                                        chan_cum_power_cost_1,
                                                        chan_curr_power_usage_2,
                                                        chan_cum_power_cost_2 ) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)""",owl_electricity.getFieldValues())     
            self.conn.commit()       
        except:
            ## rollback
            self.conn.rollback()
            
    def createTables(self):
        try:
            cur = self.conn.cursor()
            cur.execute("""CREATE TABLE IF NOT EXISTS owlelectricity (  device_id int, 
                                                                        time timestamp, 
                                                                        curr_power_usage real, 
                                                                        curr_power_cost_h real, 
                                                                        day_cum_power_usage real, 
                                                                        day_cum_power_cost real, 
                                                                        chan_curr_power_usage_0 real,
                                                                        chan_cum_power_cost_0 real,
                                                                        chan_curr_power_usage_1 real,
                                                                        chan_cum_power_cost_1 real,
                                                                        chan_curr_power_usage_2 real,
                                                                        chan_cum_power_cost_2 real,
                                                                        PRIMARY KEY (device_id, time) )""")  
            self.conn.commit()
        except:
            ## rollback
            self.conn.rollback()

class OwlObject:
    def __init__(self,xml=None):
        if xml is not None:
            self.load(xml)

    
    def getTime(self):
        return self.time
    
    def getTimeString(self):
        return str(self.getTime())
    
    def makeDate(self,timestamp):
        return datetime.datetime.fromtimestamp(timestamp ,tz=pytz.utc)
    
    ## abstract
    def getFieldValues(self):
        pass

    ## abstract
    def loadFromRoot(self,xml_root):
        pass
    
    ## abstract
    def asFlatDict(self):
        pass
    
    def load(self,xml):
        try:
            root = xml # if it's already a XML tree
            if isinstance(xml, basestring): # if it's a string, parse
                root = ET.fromstring(xml)

            self.loadFromRoot(root)
        except AttributeError as e:
            print "BAD {1} DATA: {0}".format(xml,type(self).__name__)
            print e
    
    def __str__(self):
        return ";".join(self.getFieldValues())
    
#   <electricity id="443719101897" ver="2.0">
#     <timestamp>1465940288</timestamp>
#     <signal rssi="-67" lqi="28"/>
#     <battery level="100%"/>
#     <channels>
#       <chan id="0">
#         <curr units="w">305.00</curr>
#         <day units="wh">6258.98</day>
#       </chan>
#       <chan id="1">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="2">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="3">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="4">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#       <chan id="5">
#         <curr units="w">0.00</curr>
#         <day units="wh">0.00</day>
#       </chan>
#     </channels>
#     <property>
#       <current>
#         <watts>305.00</watts>
#         <cost>5.95</cost>
#       </current>
#       <day>
#         <wh>6258.98</wh>
#         <cost>146.96</cost>
#       </day>
#       <tariff>
#         <curr_price>0.19</curr_price>
#         <block_limit>4294967295</block_limit>
#         <block_usage>5321</block_usage>
#       </tariff>
#     </property>
#   </electricity>

class OwlElectricity(OwlObject):
    def __init__(self,xml=None):
        OwlObject.__init__(self, xml)
            
    def loadFromRoot(self, xml_root):
        self.channels = {}
            
        self.device_id = xml_root.get("id")
            
        self.timestamp = int(xml_root.find("timestamp").text)
        self.time = self.makeDate(self.timestamp) # datetime
            
        # later use
        #chan0 = root.find("channels/chan[@id='0']")
        chans = xml_root.findall("channels/chan")
        for channel in chans:
            chan_id = int(channel.get("id"))
            chan_curr_power_usage = float(channel.find("curr").text)
            chan_cum_power_cost = float(channel.find("day").text)
            self.channels[chan_id] = { "chan_curr_power_usage" : chan_curr_power_usage,
                                       "chan_cum_power_cost" : chan_cum_power_cost}
        
        self.curr_power_usage = float(xml_root.find("property/current/watts").text)
        self.curr_power_cost_h  = float(xml_root.find("property/current/cost").text) / 100 ## cent to eur/usd/gbp
        
        self.day_cum_power_usage = float(xml_root.find("property/day/wh").text)
        self.day_cum_power_cost = float(xml_root.find("property/day/cost").text) / 100 ## cent to eur/usd/gbp


    def getChannelInfo(self,channel_id):
        return self.channels[channel_id]

    def getFieldValues(self):
        values = [self.device_id,
                 self.getTime(),
                 str(self.curr_power_usage),
                 str(self.curr_power_cost_h),
                 str(self.day_cum_power_usage),
                 str(self.day_cum_power_cost)]
        for chan_id in xrange(0,3): # channels 0,1,2
            chan_info = self.getChannelInfo(chan_id)
            values.append(chan_info["chan_curr_power_usage"])
            values.append(chan_info["chan_cum_power_cost"])
        
        return values
    
    def asFlatDict(self):
        d = { 
             "curr_power_usage" : self.curr_power_usage,
             "curr_power_cost_h" : self.curr_power_cost_h,
             "day_cum_power_usage" : self.day_cum_power_usage,
             "day_cum_power_cost" : self.day_cum_power_cost
             }
        
        for chan_id in xrange(0,3): # channels 0,1,2
            chan_info = self.getChannelInfo(chan_id)
            
            d["chan_curr_power_usage_{0}".format(chan_id)] = chan_info["chan_curr_power_usage"]
            d["chan_cum_power_cost_{0}".format(chan_id)] = chan_info["chan_cum_power_cost"]

        return d
    
    
## <weather id='443719101897' code='113'><temperature>20.00</temperature><text>Clear/Sunny</text></weather>
class OwlWeather(OwlObject):
    def __init__(self,xml=None):
        OwlObject.__init__(self, xml)

    def loadFromRoot(self, xml_root):
                    
        self.device_id = xml_root.get("id")
        self.code = xml_root.get("code")
        
        temp = xml_root.find("temperature")
        self.temperature = float(temp.text)
        
        text = xml_root.find("text")
        self.text = text.text


class OwlFactory:
    owl_map = { "electricity" : OwlElectricity }
                #"weather" : OwlWeather }
    
    @classmethod
    def makeOwlObject(cl,xml):
        root = ET.fromstring(xml)
        
        root_tag = root.tag
        
        if root_tag in cl.owl_map:
            owl_class = cl.owl_map[root_tag]
            return owl_class(root)
        else: # unknown object type
            return None

## influxdb stuff

class InfluxRecord:
    def __init__(self, measurement, tags_dict, fields_dict, time):
        self.measurement = measurement
        self.tags_dict = tags_dict
        self.fields_dict = fields_dict
        self.time = time

    # cpu_load_short,host=server01,region=us-west value=0.64 1434055562000000000
    def __str__(self):
        tags_str = ",".join(["{0}={1}".format(tag_k, tag_v) for (tag_k, tag_v) in self.tags_dict.iteritems() ])
        fields_str = ",".join(["{0}={1}".format(field_k, field_v) for (field_k, field_v) in self.fields_dict.iteritems() ])

        time_ns = self.getTimeNanos()

        return "{0},{1} {2} {3}".format(self.measurement, tags_str, fields_str, time_ns)

    def getTimeNanos(self):
        epoch = datetime.datetime.utcfromtimestamp(0).replace(tzinfo=pytz.utc)

        return int((self.time - epoch).total_seconds() * 1000000000)


class InfluxAPI:
    def __init__(self, url, database):
        self.url = url
        self.database = database

    def write(self, data):

        write_api_url = "{0}/write".format(self.url)
        db_query_args = { "db" : self.database }

        if isinstance(data, list):
            post_data = "\n".join([ str(d) for d in data ])
        else:
            post_data = str(data)

        try:
            requests.post(write_api_url, params=db_query_args, data=post_data)
        except RequestException as e:
            logging.getLogger('default').error("Failed to write to influxdb: {0}".format(e))



class InfluxdbThread(StoppableThread):
    def __init__(self, influx_queue, influx_api):
        super(InfluxdbThread, self).__init__()
        self.queue = influx_queue
        self.api = influx_api
        self.name = "InfluxdbThread"
        self.daemon = True        
    
    def run(self):
        while not self.stopped():
            owl_obj = self.queue.get()
            self.queue.task_done()
            
            tags = { "host" : "owl" }
            fields = { "current_power_usage" : owl_obj.curr_power_usage,
                       "cumulative_energy_day" : owl_obj.day_cum_power_usage }
            influxrec = InfluxRecord("owl_power_usage", tags, fields, owl_obj.time)

            self.api.write(influxrec)
            logging.getLogger('default').debug("Wrote influxdb record object: {0}".format(influxrec))

     
            
            
def stop_and_wait(thread=None):
    if thread is not None:
        thread.stop()
        thread.join()
        

def main(args):
    r = None

    try:
        
        logging.basicConfig(format='%(asctime)s:%(levelname)s:%(message)s')
        logger = logging.getLogger('default')
        logger.setLevel(logging.DEBUG)
        
        the_threads= []
        
        mcast_grp = args["--multicast-group"]
        port = int(args["--port"]) 
        
        the_queue = Queue()
        influx_queue = Queue()
        
        if (args["--udp"] is False):
            logger.info("Starting Multicast listener on group {1} port {0}".format(port, mcast_grp))
            r = MulticastReader(mcast_grp,port)
        else:
            logger.info("Starting UDP listener on port {0}".format(port))
            r = UDPReader(port)
    
        reader_thread = ReaderThread(the_queue,r)
        reader_thread.start()
        the_threads.append(reader_thread)
        
        logger.info("Started reader thread")
        
        processor_thread = ProcessorThread(the_queue, dbfile, influx_queue)
        processor_thread.start()
        the_threads.append(processor_thread)
        
        logger.info("Started processor thread")
        
        influx_api = InfluxAPI("http://localhost:8086", "telegraf")
        influxdb_thread = InfluxdbThread(influx_queue, influx_api)
        influxdb_thread.start()
        the_threads.append(influxdb_thread)
        
        logger.info("Started influxdb thread")
        
        while True:
            time.sleep(1)
#         reader_thread.join()
#         processor_thread.join()

    except (KeyboardInterrupt):
#         for t in the_threads:
#             stop_and_wait(t)
        if r is not None:
            r.close()
            
    except ValueError as e:
        print e
        print "BAD port: {0}".format(args["--port"])
        sys.exit(2)
    finally:
#         for t in the_threads:
#             stop_and_wait(t)
        if r is not None:
            r.close()

if __name__ == '__main__':
    arguments = docopt(docopt_str, version=THE_VERSION)
    main(arguments)
