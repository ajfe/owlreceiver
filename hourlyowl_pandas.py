#!/usr/bin/env python
from docopt import docopt
import sqlite3
import datetime
import pytz
import pandas as pd
import numpy as np

THE_TZ="Europe/Lisbon"

docopt_str = '''hourlyowl
Prints power usage for specified period, grouped by hour

Usage:
    hourlyowl -f <from-date> -t <to-date>
    hourlyowl (-h | --help)
    hourlyowl (-v | --version)
   
Options:
    -f --from-date=<from-date>    from date (YYYY-MM-dd)
    -t --to-date=<to-date>    to date (YYYY-MM-dd)
    -h --help                       Show this screen.
    -v --version                    Show version.
    
'''

# takes a string representation of a date, returns datetime object
# if local==True, interpret string as localtime, convert to UTC
# else, interpret as UTC, convert
def str_to_dt(time_str, local=False):
    time_str_notz = time_str.split("+")[0]
    dt_naive = datetime.datetime.strptime(time_str_notz,"%Y-%m-%d %H:%M:%S")
    if local:
        local_tz = pytz.timezone (THE_TZ)
        local_dt = local_tz.localize(dt_naive, is_dst=None) 
        return local_to_utc(local_dt)
    else:
        utc = dt_naive.replace(tzinfo=pytz.utc) # make it an aware datetime
        return utc

# takes an UTC datetime object, returns a string representation
def dt_to_str(time_dt, time_fmt="%Y-%m-%d %H:%M:%S"):
    return time_dt.strftime(time_fmt)

def utc_to_local(utc_time):
    local = pytz.timezone (THE_TZ)
    local_dt = utc_time.astimezone(local)
    return local_dt

def local_to_utc(local_time):
    utc_dt = local_time.astimezone (pytz.utc)
    return utc_dt

# crap
def utc_to_local_str(utc_time_str, time_fmt="%Y-%m-%d %H:%M:%S"):
    utc = datetime.datetime.strptime (utc_time_str, time_fmt)
    local_str = utc_to_local(utc).strftime(time_fmt)
    return local_str
    



def local_to_utc_str(local_time_str, time_fmt="%Y-%m-%d %H:%M:%S"):
    local = datetime.datetime.strptime (local_time_str, time_fmt)
    utc_str = local_to_utc(local).strftime(time_fmt)
    return utc_str

class SQLiteDB:
    def __init__(self,dbfile):
        self.conn = sqlite3.connect(dbfile) #,detect_types=sqlite3.PARSE_DECLTYPES)
        self.conn.row_factory = sqlite3.Row
        
    def close(self):
        self.conn.close()

    def readElectricity(self, from_dt_utc, to_dt_utc):
        
        start_str = dt_to_str(from_dt_utc)
        end_str = dt_to_str(to_dt_utc)
        
        cur = self.conn.cursor()
        cur.execute("""select time, curr_power_usage, curr_power_cost_h, day_cum_power_usage, day_cum_power_cost 
                        from owlelectricity 
                        where datetime(time) >= ? and datetime(time) < ?""",(start_str,end_str)) ## UTC
        
#         where time >= ? and time < ?""",(1467849600,1467936000))
        
        el_data = []
        
        #el_data.append(Elec(str_to_time(start_str),0.0,0.0,0.0,0.0))
        
        rows = cur.fetchall()
        for row in rows:
            (time_str, curr_pwr, curr_cost, day_pwr, day_cost) = (row)
            time = str_to_dt(time_str)
            el = Elec(time,curr_pwr,curr_cost,day_pwr,day_cost)
            el_data.append(el)
            
        return el_data

class Elec:
    def __init__(self,time,power_usage,power_cost,day_power_usage,day_power_cost):
        self.time = time
        self.power_usage = float(power_usage)
        self.power_cost = float(power_cost)
        self.day_power_usage = float(day_power_usage)
        self.day_power_cost = float(day_power_cost)
        
    def __str__(self):
        return "time={0}, pwr_use={1}, day_pwr_use={2}".format(self.time,self.power_usage,self.day_power_usage)

class HourlyStats:
    
    def __init__(self, elecs):
        self.hourlies = {} # "2016-11-28 00" : Electricity
        self.ingest(elecs)
        
    def ingest(self, elecs):
        for elec in elecs:
            # get elec.time as YYYY-MM-dd HH == etd
            # if not self.hourlies[etd]
            #   self.hourlies[etd] = elec
            # else
            #   if elec.time > self.hourlies[etd].time
            #      self.hourlies[etd] = elec
            etd = dt_to_str(elec.time, time_fmt="%Y-%m-%d %H")
            try:
                e = self.hourlies[etd]
                
                if elec.time > e.time:
                    self.hourlies[etd] = elec
                
            except KeyError:
                self.hourlies[etd] = elec
                
    def calc(self):
        hourly_raw = []
        for etd, elec in self.hourlies.iteritems():
            hourly_raw.append({ "etd" : etd, 
                              "cum_pwr_usage" : elec.day_power_usage,
                              "hour_pwr_usage" : -1 })
            
        # sort it by etd
        hourly_raw.sort(key=lambda pif: pif["etd"]) 
        
        prev = None
        for curr in hourly_raw:
            if prev is None:
                hour_pwr_usage = curr["cum_pwr_usage"]
            else: 
                hour_pwr_usage = curr["cum_pwr_usage"] - prev["cum_pwr_usage"]
                if hour_pwr_usage < 0: # day passed
                    hour_pwr_usage = curr["cum_pwr_usage"]
            curr["hour_pwr_usage"] = hour_pwr_usage
            prev = curr
        
        self.hourly_pwr_stats = hourly_raw
        return hourly_raw

def elecs_to_dataframe(elecs):
    electuples = []
    elecfields = ['time', 'power_usage', 'power_cost', 'day_power_usage', 'day_power_cost']
    
    for e in elecs:
        tup = (e.time, e.power_usage, e.power_cost, e.day_power_usage, e.day_power_cost)
        electuples.append(tup)
        
    return pd.DataFrame(data = electuples, columns = elecfields)

def main(args):
    from_date_str = args["--from-date"]
    to_date_str = args["--to-date"]
    
    aggregation_interval="1H"
    
#     pd.set_option('display.height', 1000)
    pd.set_option('display.max_rows', 500)
    pd.set_option('display.max_columns', 500)
    pd.set_option('display.width', 1000)
    
    from_dt = str_to_dt(from_date_str, local=True)
    
    print "Args dt from {0} to {1}".format(from_date_str, to_date_str)
    
    # subtract 1h to account for non-snapping start times
    from_dt = from_dt - datetime.timedelta(hours=1)
    
    to_dt = str_to_dt(to_date_str, local=True)
    
    print "Selecting from {0} to {1}".format(from_dt, to_dt)
    
    db = SQLiteDB("owlreceiver.db")
    
    data = db.readElectricity(from_dt, to_dt) 

    elecdata = elecs_to_dataframe(data)

    grouped_by_hour = elecdata.groupby(pd.Grouper(key='time', freq=aggregation_interval)) # pandas 0.19+

    # get row with max. time in intervals    
    maxxed_too = grouped_by_hour.apply(lambda t: t[t.time == t.time.max()])
    
    # calc difference from interval to the previous one
    maxxed_too['interval_usage'] = maxxed_too['day_power_usage'] - maxxed_too['day_power_usage'].shift(1)
    
    # fix < 0 on day wrap
    maxxed_too['interval_usage'] = np.where(maxxed_too['interval_usage'] < 0, maxxed_too['day_power_usage'], maxxed_too['interval_usage'])
    
    # fix NaN on first row
    maxxed_too['interval_usage'] = np.where(pd.isnull(maxxed_too['interval_usage']), maxxed_too['day_power_usage'], maxxed_too['interval_usage'])
    
    print maxxed_too #[:,['time', 'day_power_usage', 'interval_usage']]
    
    


if __name__ == '__main__':
    arguments = docopt(docopt_str, version='Hourly Owl 0.1')
    main(arguments)