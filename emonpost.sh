#!/bin/bash

PWR=${1:-100}

APIKEY="c8dc9eb90d1596e75c4b902faf97c7ae"
NODE="owl"
JSON='{"instant_power":'$PWR'}'

curl -vvv -H "Authorization: Bearer $APIKEY" "http://emoncms.petisco/emoncms/input/post?node=$NODE&fulljson=$JSON"
